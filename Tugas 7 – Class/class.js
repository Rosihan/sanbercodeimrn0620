// Soal 1
// Release 0
class Animal {
    // Code class di sini
    constructor(name) {
        this.animalName = name
        this.animalLegs = 4
        this.animalBlood = false
    }
    get name() {
        return this.animalName
    }
    get legs() {
        return this.animalLegs
    }
    get cold_blooded() {
        return this.animalBlood
    }

}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

console.log("\n")

// Release 1

// Code class Ape dan class Frog di sini
class Ape extends Animal{
    constructor(name){
        super(name)
        this.animalLegs = 2
        this.apeYell = "Auooo"
    }
    yell(){
        console.log(`${this.apeYell}`)
    }
}

class Frog extends Animal{
    constructor(name){
        super(name)
        this.frogJump = "hop hop"
    }
    jump(){
        console.log(`${this.frogJump}`)
    }
}
 
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

console.log("\n")

// Soal 2

class Clock {
    constructor({ template }){
        this.template = template
    }
    render(){
        var date = new Date();
        var hours = date.getHours();
        var mins = date.getMinutes();
        var secs = date.getSeconds();

        if (hours < 10) hours = '0' + hours;
        if (mins < 10) mins = '0' + mins;
        if (secs < 10) secs = '0' + secs;

        var output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);

        console.log(output);
    }

    stop(){
        clearInterval(this.timer);
    }

    start(){
        this.render();
        var timer = setInterval(() => this.render(),1000);
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 